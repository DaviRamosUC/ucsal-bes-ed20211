package questoes;

import java.util.Locale;
import java.util.Scanner;

public class Q6 {

	public static void main(String[] args) {

		idh();
		
	}

	private static void idh() {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		String nome = sc.nextLine();
		double altura = sc.nextDouble();
		sc.nextLine();
		char sexo = sc.nextLine().charAt(0);
		
		double idh = (sexo=='M') ? (72.7*altura)-58 : (62.1*altura)-44.7;
		
		System.out.println("O IDH ideal para "+nome+" �: "+idh);
		
		sc.close();
	}

}
