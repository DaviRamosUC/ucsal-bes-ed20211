package questoes;

import java.util.Scanner;

public class Q4 {

	public static void main(String[] args) {
		
		operacao();
	}

	private static void operacao() {
		Scanner sc = new Scanner(System.in);
		double x , y , r = 0;
		int op;
		
		System.out.print("Informe o primeiro n�mero: ");
		x= sc.nextDouble();
		System.out.print("Informe o segundo n�mero: ");
		y= sc.nextDouble();
		do {
			System.out.print("(1)Soma \n(2)Subtra��o \n(3)Multiplica��o \n(4)Divis�o \nInforme a opera��o desejada: ");
			op=sc.nextInt();
			if (op != 1 && op != 2 && op != 3 && op != 4) {
				System.out.println("Opera��o n�o registrada!");
			}
		} while (op != 1 && op != 2 && op != 3 && op != 4);
		
		switch (op) {
		case 1:
			r = x + y;
			break;
		case 2:
			r = x - y;
			break;
		case 3:
			r = x * y;
			break;
		case 4:
			r = x / y;
			break;
		}
		
		System.out.println(r);
		
		sc.close();
	}
}
