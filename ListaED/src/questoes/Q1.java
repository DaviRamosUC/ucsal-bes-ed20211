package questoes;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {

		maior();
	}

	private static void maior() {
		Scanner sc = new Scanner(System.in);

		double x, y, m;

		x = sc.nextDouble();
		y = sc.nextDouble();
		
		m = (x > y) ? x : y;
		System.out.println(m);
		sc.close();
	}

}
