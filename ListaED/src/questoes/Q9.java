package questoes;

public class Q9 {

	public static void main(String[] args) {
		impares();
	}

	private static void impares() {
		for (int i = 1; i <= 50; i++) {
			System.out.print((i%2!=0) ? i:" ");
		}
	}

}
