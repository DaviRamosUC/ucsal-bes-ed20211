package questoes;

public class Q10 {

	public static void main(String[] args) {
		for (int i = 1; i <= 50; i++) {
			int counter = 0;
			for (int k = 1; k <= i; k++) {
				if (i % k == 0)
					++counter;
			}
			System.out.print((counter == 2) ? i : " ");
		}
	}

}
