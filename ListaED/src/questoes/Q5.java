package questoes;

import java.util.Scanner;

public class Q5 {

	public static void main(String[] args) {
		estoqueMedio();

	}

	private static void estoqueMedio() {
		Scanner sc = new Scanner(System.in);

		double em, qm, qma;

		String nome = sc.nextLine();
		qm = sc.nextDouble();
		qma = sc.nextDouble();
		
		em = (qm + qma) / 2;
		
		System.out.println("A quantidade m�dia do produto "+nome+" �: "+em);
		
		sc.close();
	}

}
