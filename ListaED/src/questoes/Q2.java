package questoes;

import java.util.Scanner;

public class Q2 {
	
	public static void main(String[] args) {
		
		menorMaior();
	}

	private static void menorMaior() {
		Scanner sc = new Scanner(System.in);

		double x, y, m=0, me=0;

		x = sc.nextDouble();
		y = sc.nextDouble();

		if (x==y) {
			System.out.println("Os n�meros s�o iguais");
		}else {
			System.out.println("Os n�meros s�o diferentes");
			if (x>y) {
				m=x;
				me=y;
			}else {
				m=y;
				me=x;
			}
		}
				
		System.out.println(m+" "+me);
		sc.close();
	}
}
